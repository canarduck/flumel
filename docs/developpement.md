# Développement

Pour participer au développement (ou à la documentation) de flumel il *suffit* de soumettre des changements sur le [dépôt](https://gitlab.com/canarduck/flumel)) ou de [signaler un problème](https://gitlab.com/canarduck/flumel/issues).

## Environnement

Pour installer les paquets nécessaires au développement : `pip install -r dev-requirements.txt`

## Tests et documentation

L’objectif est de disposer :

* d’une application testée unitairement et fonctionnellement
* d’une documentation toujours à jour

Merci d’y penser en soumettant des patchs.

## Journal des modifications

[CHANGELOG.md](https://gitlab.com/canarduck/flumel/raw/master/CHANGELOG.md) est à mettre à jour à l'aide de [gitchangelog](https://github.com/vaab/gitchangelog).
Un [crochet git](https://git-scm.com/book/fr/v1/Personnalisation-de-Git-Crochets-Git) est disponible pour automatiser la mise à jour du journal des modifications : [post-commit](https://gitlab.com/canarduck/flumel/raw/master/templates/post-commit). Pensez à modifier le chemin vers l'environnement virtuel python (5e ligne).

## Feuille de route

La v1 de flumel doit être simple (0 paramètre pour l’utilisateur), stable (testée), fonctionnelle (envoi d’articles lisibles, réponse correcte aux commandes) et personnalisable (chaînes de caractères, gabarits, etc.), telle que décrite sur la page d’accueil de la documentation.

D’ici là les versions ([numérotées de façon sémantique](https://semver.org/lang/fr/)) marqueront l’avancement du projet.