# Installation

Flumel est testé et développé sous linux, mais *à priori* pas de raison que cela ne fonctionne pas ailleurs.

On a besoin :

* d’un shell pour installer le paquet `flumel`
* de python 3
* d’un accès (SSL ou STARTTLS) à un serveur SMTP pour envoyer les mails
* d’un accès (SSL ou STARTTLS) à un serveur IMAP pour recevoir des commandes
* d’un compte mail dédié au bot
* d’un programme pour automatiser le lancement du gestionnaire de tâches (systemd par exemple)
* de bases SQLite pour stocker les données et la file d’attente des tâches

## Installation du paquet

En root, on crée un utilisateur dédié `flumel` (c'est facultatif, mais c'est plutôt une bonne pratique), on installe les paquets requis dans un environnement virtuel `venv`.

```sh
adduser flumel
su flumel
cd ~
python3 -m venv venv
source venv/bin/activate
pip install flumel
```

## Initialisation

`flumel-init` va lancer les 2 tâches nécessaires au premier lancement de flumel :

* `flumel-config`: génération de `flumel.cfg` à l’aide d’un système de questions/réponses
* `flumel-page`: génération de la page dédiée de l'instance qui permet d’afficher l'adresse du bot, les commandes, etc.

Ces trois commandes sont disponibles à tout moment si besoin.

### Configuration

2 possibilités : soit utiliser le `flumel.cfg` généré soit utiliser des variables d’environnement.
La conversion configparser <-> variables d’environnement est faite dans [settings.py]([flumel.service](https://gitlab.com/canarduck/flumel/raw/master/flumel/settings.py))

### Systemd

Systemd lance au démarrage de la machine [le consumer de huey](https://huey.readthedocs.io/en/latest/consumer.html) pour gérer les tâches. Pour l’activer il faut :

* récupérer le modèle [flumel.service](https://gitlab.com/canarduck/flumel/raw/master/templates/flumel.service)
* modifier les chemins pour correspondre à son installation
* le copier dans `/etc/systemd/system`
* l’activer au démarrage `sudo systemctl daemon-reload` puis `sudo systemctl enable flumel`
* le lancer `sudo systemctl start flumel`

Il est bien entendu possible d’utiliser autre chose pour gérer daemon.

### Page dédiée

Facultatif : flumel génère une page web statique toutes les 24h. Elle contient les infos pour utiliser l'instance (adresse du bot, mot clés, etc.) et quelques statistiques. La page est crée dans `web/index.html`, pour la servir dans nginx :

```conf
server {
    server_name flumel.fr www.flumel.fr;
    root /home/flumel/web;
    index index.html;
    access_log off;
    error_log off;
}
```

## Mise à jour

Mettre à jour flumel c'est récupérer la dernière version du paquet et redémarrer huey:

```sh
cd /mon/répertoire
source venv/bin/activate
pip install --upgrade flumel
sudo systemctl restart flumel
```

TODO: gérer les migrations si modification de la structure de la base