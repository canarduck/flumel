# Flumel

[![Intégration continue](https://gitlab.com/canarduck/flumel/badges/master/pipeline.svg)](https://gitlab.com/canarduck/flumel/commits/master) [![Couverture des tests](https://gitlab.com/canarduck/flumel/badges/master/coverage.svg)](https://canarduck.gitlab.io/flumel/) [![Documentation](https://readthedocs.org/projects/flumel/badge/?version=latest)](https://flumel.readthedocs.io/fr/latest/)

Encore un moyen de recevoir ses flux RSS par email

## Principe de fonctionnement

* Une instance publique est accessible sur [flumel.fr](https://flumel.fr), mais on peut également [héberger sa propre version](installation.md) et la personnaliser
* Flumel permet de s’abonner par mail à un flux RSS en envoyant un simple email (par défaut "Abonnement URLDUFLUX") au robot de l'instance.
* Pas de login ni mot de passe ni interface web (enfin si, juste une page statique pour lister les commandes et informer de l’adresse mail du robot).
* Régulièrement (chaque heure sur l’instance publique) les nouveaux articles des flux sont envoyés par email.

Le reste (durée de conservation, tri, classement, mode hors-ligne, etc.) est dans les mains de votre client mail.

### Pilotage

Le robot flumel consulte sa boite mail (en IMAP) et répond aux commandes qu’on lui transmet (voir plus bas), donc pas d’interface web, pas d'identifiants et pas de mots de passe. Une interface en lignes de commandes pour l’administration est cependant en cours de préparation. 

### Envoi des nouveaux articles

[Feedparser](https://pythonhosted.org/feedparser/) récupère les flux, [readability](https://github.com/buriy/python-readability) essaye de récupérer une meilleure version des articles (qui sont parfois tronqués dans les flux) et le résultat est envoyé par email :

* Expéditeur : "Nom du flux/site" <bot@flumel.fr>
* Sujet : titre de l'article
* Entête X-Flumel-Keywords: Mots clés (tags ou catégories) de l’article pour un tri éventuel
* Corps : un mail multipart avec l'article en HTML et en texte brut (plus exactement en markdown grace à [html2text](https://github.com/Alir3z4/html2text/))

### Bases de données

Une base SQLite stocke les infos sur les flux (url, date de dernier envoi, ...) et les abonnements (un flux + une adresse mail), une autre la file d’attente des tâches à réaliser pour [huey](https://huey.readthedocs.io).

## Commandes

Les commandes sont passées dans le titre des emails et sont insensibles à la casse. Si une commande n'est pas reconnue l'email est ignoré.
Après le passage du robot tous les messages sont supprimés, aucun historique n’est conservé.

### Abonnement

Pour ajouter un flux à ses abonnements

* Sujet : abonnement `url`
* Réponse : un message de confirmation ou d'erreur

### Désabonnement

Pour supprimer un flux de ses abonnements

* Sujet : suppression `url`
* réponse : un message de confirmation ou d'erreur

### Export (TODO)

Pour récupérer l`OPML de ses abonnements

* Sujet : export
* réponse : un message de confirmation (avec OPML en pièce jointe) ou d'erreur

## Pourquoi faire un nouveau rss2mail/newspipe

J'utilise mon lecteur de flux RSS (spaRSS) dans 90% des cas pour lire, hors ligne, des articles assez longs, que je souhaite parfois partager avec mes petits camarades.

Problème : j'ai un téléphone & une tablette et le contenu n'est pas synchronisé entre les deux. Je pourrai utiliser des sites dédiés (en saas ou auto hébergés) mais j'ai pas envie de me créer des comptes et je ne suis pas fan des clients associés.

De là est né une petite réflexion :

* Qu'est-ce qui marche bien pour synchroniser l'état de lecture de messages / articles entre plusieurs comptes ? Le protocole IMAP.
* Qu'est-ce qui gère bien le mode hors ligne ? Les clients mail
* Qu'est-ce qu'on est à peu près sur de trouver sur toutes les plateformes ? Un client mail potable
* Qu'est-ce qui propose des outils de filtrage / tri avancés ? Les clients mail ou [sieve](http://sieve.info/)

Donc j'ai besoin de convertir des flux rss en email pour les lire où je veux.

Est-ce qu'il y a déjà des logiciels qui font ça ? Oui, plein ([newspipe](http://newspipe.sourceforge.net/), [rss2mail](https://exo.org.uk/code/rss2mail/), [feeds2imap](http://bitgarten.ch/projects/feeds2imap/), ...), mais jamais exactement comme je veux :

* possibilité de récupérer le contenu complet de l'article et pas seulement un extrait
* avec peu, voire pas de configuration (pour l’utilisateur, sur le serveur c'est autre chose hein)
* pas de fonctions inutiles (gestion d'arborescence pour les flux, mots blacklistés, etc.)
* multi-utilisateurs
* ajout / suppression de flux simple, depuis un téléphone, ordinateur, tablette, ...

Voila ce qui a mené au développement de flumel.