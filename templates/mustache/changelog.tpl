{{#general_title}}
# Journal des modifications

Ce fichier est généré automatiquement après chaque modification du dépôt. 
Les numéros indiqués correspondent aux [versions taggées](https://gitlab.com/canarduck/flumel/tags).

{{/general_title}}
{{#versions}}
## {{{label}}}

{{#sections}}
### {{{label}}}

{{#commits}}
* {{{subject}}} [{{{author}}}]
{{/commits}}

{{/sections}}
{{/versions}}
