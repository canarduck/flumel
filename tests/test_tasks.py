"""
Test des tâches
Huey est desactivé pendant les unittests
"""

import unittest

# pylint: disable=W0611
from . import context
# pylint: disable=C0411
from flumel import settings
from flumel import tasks
from flumel.models import Feed, Subscription


class SubscribeTest(unittest.TestCase):
    """
    Test de la tâche subscribe
    """

    def test_subscribe_simple(self):
        "Abonnement simple"
        url = 'http://www.requiempouruntwister.com/feeds/posts/default'
        email = 'test@test.fr'
        tasks.subscribe(url, email)
        feed = Feed.get(url=url)
        self.assertIsInstance(
            Subscription.get(feed=feed, email=email), Subscription)

    def test_subscribe_invalid(self):
        "Abonnement échoue car find_feeds ne trouve rien"
        url = 'http://test.fr'
        email = 'test@test.fr'
        self.assertFalse(tasks.subscribe(url, email))

    def test_subscribe_detected(self):
        "Abonnement réussi car find_feeds en a trouvé un"
        url = 'http://ww2w.fr'
        full_url = 'http://ww2w.fr/feed/'
        email = 'test@test.fr'
        tasks.subscribe(url, email)
        feed = Feed.get(url=full_url)
        self.assertIsInstance(
            Subscription.get(feed=feed, email=email), Subscription)

    def test_subscribe_multiple(self):
        "Pas d’abonnement car find_feeds trouve plusieurs flux"
        url = 'https://www.journalduhacker.net'
        email = 'test@test.fr'
        tasks.subscribe(url, email)
        with self.assertRaises(Feed.DoesNotExist):
            Feed.get(url=url)


class UnsubscribeTest(unittest.TestCase):
    """
    Test de la tâche unsubscribe
    """

    def test_unsubscribe_simple(self):
        "Désabonnement simple"
        url = 'http://ww2w.fr/feed'
        email = 'test1@unsubscribe.fr'
        email2 = 'testbis@unsubscribe.fr'
        feed, _ = Feed.get_or_create(url=url)
        feed.subscribe(email)
        feed.subscribe(email2)  # pour éviter delete feed car plus d’abonnés
        before = feed.subscriptions.count()
        tasks.unsubscribe(url, email)
        feed = Feed.get(url=url)
        self.assertEqual(feed.subscriptions.count(), before - 1)

    def test_unsubscribe_failure(self):
        "Désabonnement pas abonné"
        url = 'http://ww2w.fr/feed'
        email = 'test2@unsubscribe.fr'
        Feed.get_or_create(url=url)
        self.assertFalse(tasks.unsubscribe(url, email))

    def test_unsubscribe_unknown_feed(self):
        "Désabonnement flux inconnu"
        url = 'http://unsubscribe.fr'
        email = 'test3@unsubscribe.fr'
        self.assertFalse(tasks.unsubscribe(url, email))


class PageTest(unittest.TestCase):
    """
    Test de la génération de page
    """

    def test_content(self):
        "contenu de la page"
        tasks.page()
        with open('web/index.html', 'r') as fp_page:
            page = fp_page.read()
        self.assertTrue(settings.SUBSCRIBE_KEYWORD in page)
        self.assertTrue(settings.UNSUBSCRIBE_KEYWORD in page)
        self.assertTrue(settings.BOT_EMAIL in page)


if __name__ == '__main__':
    unittest.main()
