"""
Test des outils
"""

import os
import unittest

# pylint: disable=W0611
from . import context
# pylint: disable=C0411
from flumel import settings
from flumel import tools


class TemplateTest(unittest.TestCase):
    """
    Test de format_template
    """

    def test_format_integrated_template(self):
        "gabarit intégré"
        url = 'http://test.fr'
        result = tools.format_template('unsubscribe_success.html', url=url)
        self.assertIn(url, result)

    def test_format_custom_template(self):
        "gabarit personnalisé"
        try:
            os.mkdir('templates')
        except FileExistsError:
            pass
        tpl = open('templates/unsubscribe_success.html', 'w')
        tpl.write('<p>$test</p>')
        tpl.close()
        test = 'XXX'
        result = tools.format_template('unsubscribe_success.html', test=test)
        self.assertIn(test, result)
        os.remove('templates/unsubscribe_success.html')


if __name__ == '__main__':
    unittest.main()
