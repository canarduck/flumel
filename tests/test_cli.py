"""
Test des commandes
"""

import os
import shutil
import unittest
from unittest.mock import patch

# pylint: disable=W0611
from . import context
# pylint: disable=C0411
from flumel import cli


class ConfigTest(unittest.TestCase):
    """
    Test de la génération du fichier de conf
    Test très basique qui vérifie uniquement la présence du fichier
    """

    @patch('builtins.input', return_value='1')
    def test_file(self, _):
        "fichier généré une première fois et édité"
        cli.generate_config()
        self.assertTrue(os.path.isfile('flumel.cfg'))
        cli.generate_config()
        self.assertTrue(os.path.isfile('flumel.cfg'))


class PageTest(unittest.TestCase):
    """
    Test de la génération de la page dédiée
    Test très basique qui vérifie uniquement la présence du fichier
    """

    @patch('builtins.input', return_value='1')
    def test_file(self, _):
        "fichier généré"
        cli.generate_page()
        self.assertTrue(os.path.isfile('web/index.html'))


class InitTest(unittest.TestCase):
    """
    Test de l’inti du projet
    Efface tout (sauf les nlk histoire de ne pas plus user leur BP...)
    et relance les tâches
    """

    @patch('builtins.input', return_value='1')
    def test_result(self, _):
        "tout est généré ?"
        try:
            os.remove('flumel.cfg')
        except FileNotFoundError:
            pass
        try:
            shutil.rmtree('web')
        except FileNotFoundError:
            pass
        cli.init()
        self.assertTrue(os.path.isfile('flumel.cfg'))
        self.assertTrue(os.path.isfile('web/index.html'))


if __name__ == '__main__':
    unittest.main()
