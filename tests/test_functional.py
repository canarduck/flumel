"""
Tests fonctionnels de flumel

TODO:
refacto les tests dans test_tasks et écrire de vrais tests fonctionnels :

Test 1
* Je m’abonne au flux X
* Je reçois les derniers articles par mail
* Je suis le lien en pied de mail pour me désabonner

Test 2
* Je m'abonne à un site
* Je reçois le mail qui liste les flux disponibles
* Je m'abonne au premier
* Je reçois les derniers articles par mail
"""

import time
import unittest
from datetime import datetime

import feedparser
from fake_useragent import UserAgent

from flumel import settings, tasks
from flumel.models import Feed, Subscription
from flumel.tools import IMAP, SMTP, decode_email_header

# pylint: disable=W0611
from . import context

SLEEP = 4  # délais d’attente pour laisser les mails arriver


class IMAPDebug(IMAP):
    """
    Ajout d'une méthode de recherche dans les mails de la boite
    Utilisé pour les tests
    """

    def search_by_title(self, needle: str):
        """
        Retourne l'id du premier mail qui correspond à la recherche
        """
        print('Recherche ' + needle)
        for number in self.inbox:
            message = self.get(number)
            haystack = decode_email_header(message['Subject'])
            if needle in haystack:
                return number
            print('- Introuvable dans ' + haystack)


class ServerCase(unittest.TestCase):
    """
    Classe de base pour les tests qui passent par les serveurs
    """

    imap = None
    smtp = None
    email = settings.BOT_EMAIL

    @classmethod
    def setUpClass(cls):
        cls.imap = IMAPDebug()
        cls.smtp = SMTP()

    @classmethod
    def tearDownClass(cls):
        cls.imap.close()
        cls.smtp.close()

    def setUp(self):
        """
        Purge imap avant chaque test
        """
        self.imap.purge()


class TestSubscribe(ServerCase):
    """
    Test des mécanismes d'abonnement
    """

    def test_invalid_feed(self):
        "Flux invalide"
        url = 'http://www.test.fr'
        subject = '{keyword} {url}'.format(
            keyword=settings.SUBSCRIBE_KEYWORD, url=url)
        self.smtp.send(self.email, subject, '')
        tasks.management()
        with self.assertRaises(Feed.DoesNotExist):
            Feed.get(url=url)
        for _ in range(3):
            number = self.imap.search_by_title(
                'Flux RSS non trouvé sur {url}'.format(url=url))
            if number:
                break
            time.sleep(SLEEP)
        self.assertTrue(number)

    def test_valid_feed(self):
        "Flux valide"
        url = 'https://news.google.com/news/rss/'
        subject = '{keyword} {url}'.format(
            keyword=settings.SUBSCRIBE_KEYWORD, url=url)
        self.smtp.send(self.email, subject, '')
        tasks.management()
        time.sleep(SLEEP)
        feed = Feed.get(url=url)
        self.assertIsInstance(feed, Feed)  # flux créé en base
        subscription = Subscription.get(feed=feed, email=self.email)
        self.assertIsInstance(subscription, Subscription)  # abonnement
        for _ in range(3):
            number = self.imap.search_by_title(
                'Abonnement réussi à {title}'.format(title=feed.title))
            if number:
                break
            time.sleep(SLEEP)
        self.assertTrue(number)

    def test_duplicate_feed(self):
        "abonnement impossible car déjà abonné"
        url = 'http://rss.liberation.fr/rss/9/'
        feed = Feed.create(url=url)
        feed.subscribe(self.email)
        time.sleep(SLEEP)
        self.imap.purge()  # effacement mail confirmation inscription
        subject = '{keyword} {url}'.format(
            keyword=settings.SUBSCRIBE_KEYWORD, url=url)
        self.smtp.send(self.email, subject, '')
        tasks.management()
        for _ in range(3):
            number = self.imap.search_by_title(
                'Déjà abonné à {title}'.format(title=feed.title))
            if number:
                break
            time.sleep(SLEEP)
        self.assertTrue(number)


class TestUnsubscribe(ServerCase):
    """
    Test des mécanismes de désabonnement
    """

    def test_unsubscribe_valid(self):
        "Désabonnement normal"
        url = 'http://rss.liberation.fr/rss/9/'
        feed, _ = Feed.get_or_create(url=url)
        feed.subscribe(self.email)
        time.sleep(SLEEP)
        tasks.management()  # effacement mail confirmation inscription
        subject = '{keyword} {url}'.format(
            keyword=settings.UNSUBSCRIBE_KEYWORD, url=url)
        self.smtp.send(self.email, subject, '')
        tasks.management()
        for _ in range(3):
            needle = 'Désabonnement du flux {url}'.format(url=url)
            number = self.imap.search_by_title(needle)
            if number:
                break
            time.sleep(SLEEP)
        self.assertTrue(number)

    def test_unsubscribe_unknown(self):
        "Désabonnement flux inconnu"
        url = 'http://www.lemonde.fr/rss/une.xml'
        subject = '{keyword} {url}'.format(
            keyword=settings.UNSUBSCRIBE_KEYWORD, url=url)
        self.smtp.send(self.email, subject, '')
        tasks.management()
        for _ in range(3):
            needle = 'Échec de désabonnement au flux {url}'.format(url=url)
            number = self.imap.search_by_title(needle)
            if number:
                break
            time.sleep(SLEEP)
        self.assertTrue(number)

    def test_unsubscribe_notsubscribed(self):
        "Désabonnement flux auquel on est pas abonné"
        url = 'http://www.france24.com/fr/actualites/rss'
        Feed.create(url=url, title='f24')
        subject = '{keyword} {url}'.format(
            keyword=settings.UNSUBSCRIBE_KEYWORD, url=url)
        self.smtp.send(self.email, subject, '')
        tasks.management()
        for _ in range(3):
            needle = 'Échec de désabonnement au flux {url}'.format(url=url)
            number = self.imap.search_by_title(needle)
            if number:
                break
            time.sleep(SLEEP)
        self.assertTrue(number)


class TestSubscriptions(ServerCase):
    """
    Test des mécanismes de vérification des flux
    """

    def test_send_subscriptions(self):
        "Envoi des articles"
        user_agent = UserAgent()
        feedparser.USER_AGENT = user_agent.ff
        Feed.delete().execute()
        url = 'http://www.lepoint.fr/24h-infos/rss.xml'
        feed = Feed.create(url=url, title='Le point')
        feed.subscribe(self.email)
        parsed_feed = feedparser.parse(feed.url)
        needle = parsed_feed.entries[0].title
        tasks.subscriptions()
        for _ in range(3):
            number = self.imap.search_by_title(needle)
            if number:
                break
            time.sleep(SLEEP)
        self.assertTrue(number)

    def test_sent_at(self):
        "si sent_at >= date dernier article, rien n’est envoyé"
        Feed.delete().execute()
        url = 'https://www.lesechos.fr/rss/rss_une.xml'
        feed = Feed.create(url=url, title='Les echos')
        feed.subscribe(self.email)
        feed.sent_at = datetime.now()
        feed.save()
        time.sleep(SLEEP)
        self.imap.purge()
        tasks.subscriptions()
        self.assertEqual(len(self.imap.inbox), 0)


if __name__ == '__main__':
    unittest.main()
