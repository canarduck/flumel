"""
Test des commandes
"""

import unittest
from datetime import datetime, timedelta

from flumel.models import (BLOCK_BLACKLIST_TRIGGER, BLOCK_RELEASE_DELAY,
                           BLOCK_RESET_DELAY, SOFT_BLOCK_TRIGGER,
                           SOFT_RESET_DELAY, Bounce, Feed)

# pylint: disable=W0611
from . import context


class FeedTest(unittest.TestCase):
    """
    Tests du modèle des flux
    """

    def test_subscribe_ok(self):
        "Abonnement simple"
        feed = Feed.create(url='http://www.test1.fr', title='test1')
        self.assertTrue(feed.subscribe('first@email.fr'))
        self.assertEqual(feed.subscriptions.count(), 1)

    def test_subscribe_ko(self):
        "Abonnement en doublon"
        feed = Feed.create(url='http://www.test2.fr', title='test2')
        feed.subscribe('second@email.fr')
        self.assertFalse(feed.subscribe('second@email.fr'))
        self.assertEqual(feed.subscriptions.count(), 1)

    def test_unsubscribe_ok(self):
        "Désabonnement valide et suppression du flux"
        feed = Feed.create(url='http://www.test3.fr', title='test3')
        feed.subscribe('third@email.fr')
        self.assertTrue(feed.unsubscribe('third@email.fr'))
        with self.assertRaises(Feed.DoesNotExist):
            Feed.get(url='http://www.test3.fr')

    def test_unsubscribe_ko(self):
        "Désabonnement innexistant"
        feed = Feed.create(url='http://www.test4.fr', title='test4')
        feed.subscribe('four@email.fr')
        self.assertFalse(feed.unsubscribe('five@email.fr'))


class BounceTest(unittest.TestCase):
    """
    Tests du modèle des rejets mails
    """

    def test_soft_incremented(self):
        "Incrémentation rejet temporaire"
        bounce = Bounce.create(email='one@email.fr')
        stamp = datetime.now() + timedelta(days=SOFT_RESET_DELAY)
        bounce.soft()
        self.assertEqual(bounce.soft_count, 1)
        self.assertGreaterEqual(bounce.soft_reset_at, stamp)

    def test_soft_triggered(self):
        "Blocage suite trop de rejets temporaires"
        bounce = Bounce.create(
            email='two@email.fr', soft_count=SOFT_BLOCK_TRIGGER)
        stamp_release = datetime.now() + timedelta(days=BLOCK_RELEASE_DELAY)
        stamp_reset = datetime.now() + timedelta(days=BLOCK_RESET_DELAY)
        bounce.soft()
        self.assertTrue(bounce.is_blocked)
        self.assertFalse(bounce.is_blacklisted)
        self.assertGreaterEqual(bounce.block_reset_at, stamp_reset)
        self.assertGreaterEqual(bounce.block_release_at, stamp_release)

    def test_soft_ignored_blocked(self):
        "Non incrémentation rejet temporaire quand déjà bloqué"
        bounce = Bounce.create(email='three@email.fr', is_blocked=True)
        bounce.soft()
        self.assertEqual(bounce.soft_count, 0)

    def test_soft_ignored_blacklisted(self):
        "Non incrémentation rejet temporaire quand blacklisté"
        bounce = Bounce.create(email='four@email.fr', is_blacklisted=True)
        bounce.soft()
        self.assertEqual(bounce.soft_count, 0)

    def test_hard(self):
        "Rejet permanent"
        bounce = Bounce.create(email='five@email.fr')
        stamp_release = datetime.now() + timedelta(days=BLOCK_RELEASE_DELAY)
        stamp_reset = datetime.now() + timedelta(days=BLOCK_RESET_DELAY)
        bounce.hard()
        self.assertTrue(bounce.is_blocked)
        self.assertEqual(bounce.block_count, 1)
        self.assertFalse(bounce.is_blacklisted)
        self.assertGreaterEqual(bounce.block_reset_at, stamp_reset)
        self.assertGreaterEqual(bounce.block_release_at, stamp_release)

    def test_blacklist_triggered(self):
        "Liste noire suite trop de blocages"
        bounce = Bounce.create(
            email='six@email.fr', block_count=BLOCK_BLACKLIST_TRIGGER)
        bounce.hard()
        self.assertTrue(bounce.is_blacklisted)

    def test_block_ignored_blocked(self):
        "Non incrémentation block quand déjà bloqué"
        bounce = Bounce.create(
            email='seven@email.fr', is_blocked=True, block_count=1)
        bounce.hard()
        self.assertEqual(bounce.block_count, 1)

    def test_block_ignored_blacklisted(self):
        "Non incrémentation block quand déjà blacklisté"
        bounce = Bounce.create(
            email='eight@email.fr', is_blacklisted=True, block_count=3)
        bounce.hard()
        self.assertEqual(bounce.block_count, 3)


if __name__ == '__main__':
    unittest.main()
